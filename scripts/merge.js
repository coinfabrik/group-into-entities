var args = process.argv,
  level = require('levelup'),
  db1 = level('../data/' + args[2]),
  db2 = level('../data/' + args[3]),
  db2KeyCount = args[4];

var operations = [];
const BATCH_SIZE = 10000;

console.time('Total time:');
db2.createReadStream()
  .on('data', function (data) {
    operations.push({type:'put', key: data.key, value: data.value});
    if (operations.length >= BATCH_SIZE) {
      db1.batch(operations, () => {
        db2KeyCount -= BATCH_SIZE;
        console.log(db2KeyCount);
      });
      operations = [];
    }
  })
  .on('error', function (err) {
    console.log('Oh my!', err)
  })
  .on('close', function () {
    console.log('Stream closed')
  })
  .on('end', function () {
    var count = operations.length;
    if (count > 0) {
      db1.batch(operations, () => {
        db2KeyCount -= count;
        console.log(db2KeyCount);
      });
    }
    console.log('Stream ended');
    console.timeEnd('Total time:');
  });

