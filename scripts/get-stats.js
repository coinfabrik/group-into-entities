var dbName = process.argv[2];
if (!dbName) {
  console.log('Usage:\nnode get-stats <database name> [<encoding (json)>]');
  process.exit();
}
var leveldown = require('leveldown')('../data/' + dbName);
leveldown.open(function() {
  console.log('Stats:\n' + leveldown.getProperty('leveldb.stats'));
});

