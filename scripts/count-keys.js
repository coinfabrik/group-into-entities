var lvl = require('levelup');
var dbName = process.argv[2];
if (!dbName) {
  console.log('Usage:\nnode count-keys <database name>');
  process.exit();
}
var db = lvl('../data/' + dbName, {
});

var count = 0;
var finished = false;

db.createKeyStream()
  .on('data', function (data) {
    count++;
    console.log(count);
    if (finished) {
      console.log('Post finished count: ' + count);
    }
  })
  .on('error', function (err) {
    console.log('Oh my!', err)
  })
  .on('close', function () {
    console.log('Stream closed')
  })
  .on('end', function () {
    console.log('Stream closed');
    console.log('Count: ' + count);
    finished = true;
  });
 //Count: txs-db 84530957

//Count: txs-db-4 7,388,020
//Count: txs-db-4b 1,388,151
//Count: txs-db-4c 675017
//Count: txs-db-4d 641616

// out-150-343 124330323
// out-313-343 23285000