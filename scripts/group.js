var lvl = require('levelup');

var inputsDb = lvl('../data/txs-in', {
  valueEncoding: 'json'
});
var groupsDb = lvl('../data/groups', {});

require('../group').groupInputs(inputsDb, groupsDb, err => {
  if (err) throw err;
  console.log('Done grouping inputs.');
});
