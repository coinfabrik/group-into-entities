var lvl = require('levelup');
var dbName = process.argv[2];
if (!dbName) {
  console.log('Usage:\nnode read <database name> [<encoding (json)>]');
  process.exit();
}
var db = lvl('../data/' + dbName, {
	valueEncoding: process.argv[3] || 'json'
});

db.createReadStream()
  .on('data', function (data) {
    console.log(data.key, '=', data.value)
  })
  .on('error', function (err) {
    console.log('Oh my!', err)
  })
  .on('close', function () {
    console.log('Stream closed')
  })
  .on('end', function () {
    console.log('Stream closed')
  });
 