var fast = require('fast.js'),
  bitcore = require('bitcore-lib'),
  async = require('async'),
  startFile = process.argv[2],
  endFile = process.argv[3],
  parser = require('./get-blocks').createParser({
    startFile: startFile,
    endFile: endFile,
    bitcoinDir: 'D:/bitcoin-blockchain/'
  });

var lvl = require('leveldown');

var dbSuffix = process.argv[4];
if(!dbSuffix) {
  dbSuffix = startFile + '-' + endFile;
}

var db = lvl('./data/out-' + dbSuffix);

function closeDb(cb) {
  console.log( "\n Closing LevelDB ..." );
  db.close(function(err) {
    if(err) throw err;
    console.log('Database closed.');
    cb();
  });
}

process.on( 'SIGINT', function() {
  closeDb(err =>  process.exit());
});


function processBlock(err, block) {
  if (err) throw err;
  if (block == null) {
    closeDb(err =>  process.exit());
    return;
  }
  var tasksCompleted = 0,
    nextBlock;

  function taskDone(err) {
    if (err) throw err;
    tasksCompleted++;
    if (tasksCompleted == 2 && nextBlock !== null) {
      async.setImmediate(() => processBlock(null, nextBlock));
    }
  }

  parser.getNextBlock((err, block) => {
    nextBlock = block;
    taskDone();
  });
  var ops = [];
  fast.forEach(block.rawTransactions, function(raw) {
    var tx = new bitcore.Transaction(raw);
    fast.forEach(tx.outputs, (o, index) => {
      var script = o.script;
      if (!script) {
        return;
      }
      var address = script.toAddress();
      if (!address) {
        return;
      }
      ops.push({type:'put', key: tx.id + index, value: address.toString()});
    });
  });
  db.batch(ops, taskDone);
}
db.open(() => parser.getNextBlock(processBlock));
