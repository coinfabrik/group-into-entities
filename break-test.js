var getBlocks = require('./get-blocks');

process.on( 'SIGINT', function() {
  console.log( "\n Closing ..." );
  process.exit( );
});

getBlocks(function(block) {
  console.log(block.time);
});
