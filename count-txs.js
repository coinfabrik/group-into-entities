var parser = require('./get-blocks').createParser({
  bitcoinDir: require('./config').bitcoinDataDir
}),
  async = require('async');

var count = 0;

function processBlock(err, block) {
  if (block == null) {
    console.log('Final count: ' + count);
    return;
  }
  count += block.rawTransactions.length;
  async.setImmediate(() => parser.getNextBlock(processBlock));
}

parser.getNextBlock(processBlock);

//Final count: 84872230