var getBlocks = require('./get-blocks-classic'),
  fast = require('fast.js'),
  bitcore = require('bitcore-lib');

var lvl = require('levelup');

var db = lvl('./data/txs-db-5');

function closeDb(cb) {
  console.log( "\n Closing LevelDB ..." );
  db.close(function(err) {
    if(err) throw err;
    console.log('Database closed.');
    cb();
  });
}

process.on( 'SIGINT', function() {
  closeDb(err =>  process.exit());
});


getBlocks(function(block) {
  fast.forEach(block.rawTransactions, function(raw) {
    var tx = new bitcore.Transaction(raw);
    var operations = [];
    fast.forEach(tx.outputs, function(o, index) {
      var script = o.script;
      if (!script) {
        return;
      }
      var address = script.toAddress();
      if (!address) {
        return;
      }
      return operations.push({type:'put', key: tx.id + index, value: address.toString()});
    });
    db.batch(operations, function(err) {
      if(err) throw err;
    });
  });

}, function(){
  closeDb(err => {});
});
