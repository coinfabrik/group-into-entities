var async = require('async'),
  assert = require('assert'),
  group = require('../group'),
  lvl = require('levelup'),
  lvldown = require('leveldown');


var mockInputDb, mockGroupDb;

before(function(done){
  lvldown.destroy('./test-inputs', err => {
    if (err) throw err;
    lvldown.destroy('./test-groups', err => {
      if (err) throw err;
      mockGroupDb = lvl('./test-groups', {});
      mockInputDb = lvl('./test-inputs', {
        valueEncoding: 'json'
      });
      var ops = [
        {type: 'put', key: 'tx1', value: ['1aaa', '1bbb']},
        {type: 'put', key: 'tx2', value: ['1bbb', '1ccc']},
        {type: 'put', key: 'tx3', value: ['1ddd']}
      ];
      mockInputDb.batch(ops, done);
    });
  });

});
describe('group#groupInputs', function(){
  it('should separate inputs into the correct groups', function(done){
    var addresses = ['1aaa', '1bbb', '1ccc', '1ddd'];
    function groupAndCheck(done) {
      group.groupInputs(mockInputDb, mockGroupDb, function(err) {
        if (err) throw err;
        var getGroups = addresses.map(addr => function (cb) {
          group.getGroup(mockGroupDb, addr, cb);
        });
        async.parallel(getGroups, function(err, groups) {
          if (err) throw err;
          console.log('Groups: ', groups);
          assert(groups[0] === groups[1] && groups[1] === groups[2] && groups[0] !== groups[3]);
          done();
        });
      });
    }
    async.series([groupAndCheck, groupAndCheck], done);//going through it twice should not break it
  });
});