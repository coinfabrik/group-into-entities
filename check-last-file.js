//Checks which file was the last loaded in the database

var getBlocks = require('./get-blocks-classic'),
  bitcore = require('bitcore-lib'),
  lvl = require('levelup');

var db = lvl('./data/txs-db-4', {
  valueEncoding: 'json'
});

getBlocks(block => {
  var txs = block.rawTransactions[0];
  //if any txn is found move out, if not, report.
  tx = new bitcore.Transaction(tx);
  var key =  tx.id + '0';
  db.get(key, (err, value) => {
    if (err) {
      if (err.notFound) {
        console.log('Transaction not found: ' + tx.id);
      } else {
        throw err;
      }
    } else {
      console.log('Transaction found.');
    }
    process.exit();
  })
}, () => {

});