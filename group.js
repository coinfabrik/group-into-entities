var fast = require('fast.js'),
  async = require('async');



function getNodeKey(groupsDb, key, callback) {
  groupsDb.get(key, function(err, data) {
    if (err) {
      if (err.notFound) {
        callback(null, key);
      } else {
        callback(err);
      }
      return;
    }
    async.setImmediate(() => {
      getNodeKey(groupsDb, data, callback);
    });
  });
}

exports.groupInputs = function groupInputs(inputsDb, groupsDb, callback) {
  groupsDb.get('lastNode', (err, value) => {
    var lastNode;
    if (err) {
      if (err.notFound) {
        lastNode = 0;
      } else {
        throw err;
      }
    } else {
      lastNode = value;
    }
    var queue = async.queue(function (data, callback) {
      var inputs = data.value;
      async.map(inputs, (key, callback) => getNodeKey(groupsDb, key, callback), (err, keys) => {
        lastNode++;
        var ops = fast.map(keys, k => ({type: 'put', key: k, value: 'n' + lastNode.toString(16)}));
        ops.push({type: 'put', key: 'lastNode', value: lastNode});
        groupsDb.batch(ops, callback);
      });
    });
    queue.drain = callback;
    inputsDb.createReadStream()
      .on('data', queue.push)
      .on('error', function (err) {
        console.log('Oh my!', err);
        callback(err);
      });
  });

};

exports.getGroup = getNodeKey;