var parser = require('./get-blocks').createParser({
    bitcoinDir: require('./config').bitcoinDataDir,
    startFile: 0,
    endFile: 0
  }),
  async = require('async'),
  fast = require('fast.js'),
  lvl = require('levelup'),
  bitcore = require('bitcore-lib'),
  db = lvl('./data/txs-db-5', {});


function processBlock(err, block) {
  if (block == null) {
    return;
  }
  var keys = [];
  fast.forEach(block.rawTransactions, raw => {
    var tx = new bitcore.Transaction(raw);
    fast.forEach(tx.outputs, (o, index) => {
      var script = o.script;
      if (!script) {
        return;
      }
      var address = script.toAddress();
      if (!address) {
        return;
      }
      //the address should be in the database
      keys.push(tx.id + index);
    });
  });
  var toProcess = keys.length,
    processed = 0;
  if (toProcess == 0) {
    async.setImmediate(() => parser.getNextBlock(processBlock));
  }
  fast.forEach(keys, k => {
    db.get(k, (err, address) => {
      if (err) throw err;
      console.log('gotten address ' + address);
      processed++;
      if (processed == toProcess) {
        parser.getNextBlock(processBlock);
      }
    });
  });
}

parser.getNextBlock(processBlock);