var lvl = require('leveldown');

var db = lvl('./index');

var blockHash = new Buffer('623793df0401ece34fe57c99bd1d6556cb5c1d27a30b69a12ecc07000000000000', 'hex');
console.log(blockHash.toString('hex'));
/*

 key= 6202ac2065e551be6b795367740c0113a926828f83f7d5c5591b020c0000000000
 key= 6202ac218dae5615a6ce6a917b1fe3b20beccbcc4bca3e52bb0000000000000000
 key= 6202ac3b9e95de7f2dac32e82264074341298ab3a6ea72ab300400000000000000

 */

function bufferReader(buffer) {
  var index = 0;
  return {
    read: function read(bytes) {
      if (index + bytes > buffer.length) {
        return null;
      }
      var result = buffer.slice(index, index + bytes);
      index += bytes;
      return result;
    }
  }
}

function readVarInt(stream) {
  var size = stream.read(1);
  var sizeInt = size.readUInt8();
  if (sizeInt < 253) {
    return size;
  }
  var add;
  if (sizeInt == 253) add = 2;
  if (sizeInt == 254) add = 4;
  if (sizeInt == 255) add = 8;
  if (add) {
    return Buffer.concat([size, stream.read(add)], 1 + add);
  }
  return -1;
}

function toInt(varInt) {
  if (!varInt) {
    return -1;
  }
  if (varInt[0] < 253) return varInt.readUInt8();
  switch(varInt[0]) {
    case 253: return varInt.readUIntLE(1, 2);
    case 254: return varInt.readUIntLE(1, 4);
    case 255: return varInt.readUIntLE(1, 8);
  }
}


function BlockHeader(reader){
  this.size =reader.read(4).toString('hex');
  this.version = reader.read(4).readUInt32LE();
  this.previousHash = reader.read(32).toString('hex');
  this.merkleHash = reader.read(32).toString('hex');
  this.time = reader.read(4).readUInt32LE();
  this.bits = reader.read(4).readUInt32LE();
  this.nonce = reader.read(4).readUInt32LE();
}

db.open(function(err) {
  if(err) {
    console.log('Error opening database.', err);
    return;
  }
  db.get(blockHash, function(err, data) {
    if (err) throw err;
    console.log(data.toString('hex'));
    var reader = bufferReader(data);
    var header = new BlockHeader(reader);
    //console.dir(header);
    var height = reader.read(2);
    console.log('height:' + height.toString('hex')  + ': BE:' + height.readUIntBE(0, 2) +
      ' LE:' + height.readUIntLE(0, 2));
  })
});