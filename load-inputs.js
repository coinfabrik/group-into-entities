var fast = require('fast.js'),
  bitcore = require('bitcore-lib'),
  async = require('async'),
  lvl = require('levelup'),
  startFile = process.argv[2],
  endFile = process.argv[3],
  databaseSuffix = process.argv[4],
  parser = require('./get-blocks').createParser({
    startFile: startFile,
    endFile: endFile,
    bitcoinDir: 'D:/bitcoin-blockchain/'
  }),
  outputsDB = lvl('./data/out-0-149-copia', {
  }),
  inputsDB = lvl('./data/in-' + (databaseSuffix ? databaseSuffix : (startFile + '-' + endFile)), {
    valueEncoding: 'json'
  });


function closeDb(cb) {
  console.log( "\n Closing LevelDB ..." );
  console.log('Not found count: ' + notFoundCount);
  outputsDB.close(function(err) {
    if(err) throw err;
    console.log('Database closed.');
    cb(null);
  });
}

process.on( 'SIGINT', function() {
  closeDb(err =>  process.exit());
});
var notFoundCount = 0;

function getAddresses(inputs, callback) {
  var addresses = [];
  fast.forEach(inputs, function(i) {
    var txid = i.prevTxId.toString('hex');
    if (txid == '0000000000000000000000000000000000000000000000000000000000000000') {
      if (inputs.length > 1) {
        throw 'Coinbase input should be the only input';
      }
      callback(null, ['coinbase']);
    } else {
      outputsDB.get(txid + i.outputIndex, function(err, address) {
        if (err) {
          if (err.notFound) {
            addresses.push('not standard');
          } else {
            throw err;
          }
        } else {
          addresses.push(address);
        }
        if (addresses.length == inputs.length) {
          callback(null, addresses);
        }
      });
    }

  });
}

function processBlock(err, block) {
  if (err) {
    throw err;
  }
  if (block == null) {
    console.log('Finished!');
    return;
  }
  var written = 0;
  var toWrite = block.rawTransactions.length;
  fast.forEach(block.rawTransactions, function(raw) {
    var tx = new bitcore.Transaction(raw);
    getAddresses(tx.inputs, addresses => {
      inputsDB.put('t' + tx.id + ':i', addresses, () => {
        written++;
        if (written == toWrite) {
          parser.getNextBlock(processBlock);
        }
      });
    });
  });
}

parser.getNextBlock(processBlock);
