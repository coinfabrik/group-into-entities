var fs = require('fs');

function readVarInt(stream) {
  var size = stream.read(1);
  var sizeInt = size.readUInt8();
  if (sizeInt < 253) {
    return size;
  }
  var add;
  if (sizeInt == 253) add = 2;
  if (sizeInt == 254) add = 4;
  if (sizeInt == 255) add = 8;
  if (add) {
    return Buffer.concat([size, stream.read(add)], 1 + add);
  }
  return -1;
}

function toInt(varInt) {
  if (!varInt) {
    return -1;
  }
  if (varInt[0] < 253) return varInt.readUInt8();
  switch(varInt[0]) {
    case 253: return varInt.readUIntLE(1, 2);
    case 254: return varInt.readUIntLE(1, 4);
    case 255: return varInt.readUIntLE(1, 8);
  }
}


function getRawTx(reader) {
  var txParts = [];
  txParts.push(reader.read(4)); //Version

  //Inputs
  var inputCount = readVarInt(reader);
  txParts.push(inputCount);
  for(var i = toInt(inputCount) - 1; i >= 0; i--) {
    txParts.push(reader.read(32)); //Previous tx
    txParts.push(reader.read(4)); //Index
    var scriptLength = readVarInt(reader);
    txParts.push(scriptLength);
    txParts.push(reader.read(toInt(scriptLength))); //Script Sig
    txParts.push(reader.read(4)); //Sequence Number
  }

  //Outputs
  var outputCount = readVarInt(reader);
  txParts.push(outputCount);
  for(i = toInt(outputCount) - 1; i >= 0; i--) {
    txParts.push(reader.read(8)); //Value
    var scriptLen = readVarInt(reader);
    txParts.push(scriptLen);
    txParts.push(reader.read(toInt(scriptLen))); //ScriptPubKey
  }
  txParts.push(reader.read(4)); //Lock time

  return Buffer.concat(txParts);
}

function bufferReader(buffer) {
  var index = 0;
  return {
    read: function read(bytes) {
      if (index + bytes > buffer.length) {
        return null;
      }
      var result = buffer.slice(index, index + bytes);
      index += bytes;
      return result;
    },
    getIndex: () => index
  }
}

function readHeader(reader) {
  var version = reader.read(4);
  if (version == null) {
    return null;
  }
  if (version.toString('hex') == 'f9beb4d9') {
    //It's actually the magic number of a different block (previous one was empty)
    reader.read(4); //block size
    return readHeader(reader);
  }
  reader.read(64); //previous hash + merkle hash
  var time = reader.read(4); //time
  if (time == null) {
    return null;
  }
    //console.log((new Date(time.readUInt32LE() * 1000)).toDateString());
  reader.read(8); //bits + nonce
  return {
    time: time.readUInt32LE()
  }
}

function readBlock(reader) {
  var magic = reader.read(4),
      blockSize = reader.read(4),
      blockHeader = readHeader(reader);
    if (blockHeader == null) {
      return null;
    }
    var txCount = toInt(readVarInt(reader)),
      txs = [];
    for(var j = 0; j < txCount; j++) {
      txs.push(getRawTx(reader));
    }
    return {
      time: blockHeader.time,
      rawTransactions: txs
    };
}

exports.createParser = function createParser(options) {
  var fileNumber = options.startFile || 0,
    endFile = options.endFile,
    bitcoinDataDir = options.bitcoinDir,
    reader,
    firstTime = true;
  return {
    getNextBlock: function getNextBlock(onBlock) {
      if (!reader) {
        if (endFile !== undefined && fileNumber > endFile) {
          onBlock(null, null); //finish
          return;
        }
        var path = bitcoinDataDir + 'blocks/blk' + ('0000' + fileNumber).slice(-5) + '.dat';
        if (!firstTime) {
          console.timeEnd('... it took');
        }
        console.log('Processing ' + path + ' ...');
        console.time('... it took');
        fs.readFile(path, function(err, data) {
          if (err) {
            if (firstTime) {
              throw 'File ' + path + ' not found. Set up your bitcoin data directory in config.js ' + err.toString();
            } else {
              console.log('finish!');
              onBlock(null, null); //finish
            }
            return;
          }
          reader = bufferReader(data);
          fileNumber++;
          firstTime = false;
          getNextBlock(onBlock);
        });
        return;
      }
      var block = readBlock(reader);
      if (!block) {
        reader = null;
        getNextBlock(onBlock);
        return;
      }
      onBlock(null, block);
    }
  };
};




